DECKS:=$(dir $(wildcard */deck.slide))
HTML:=$(addsuffix index.html, $(addprefix public/,$(DECKS)))

public/%/index.html: %/deck.slide
	represent -src $(<D) -publish $(@D)
	mv public/$*/deck.html public/$*/index.html

.PHONY: public
dist: $(HTML)

.PHONY: clean
clean:
	rm -rf public
