# Slashdeploy Decks

This repo contains presentations for [Slashdeploy][].

Presentations are built with [present][] and exported to HTML with
[represent][].

[slashdeploy]: http://slashdeploy.com
[present]: https://godoc.org/golang.org/x/tools/present
[represent]: https://github.com/cmars/represent
