require 'delegate' # Underused and powerful library // HL
require 'logger'

class NamedLogger < DelegateClass(Logger)
  def initialize(logger, progname)
    super logger
    @progname = progname
  end

  def info(msg)
    super(@progname) { msg }
  end
end
