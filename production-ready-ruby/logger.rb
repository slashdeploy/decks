require 'logger'

logger = Logger.new($stdout).tap do |log|
  log.level = ENV.fetch('LOG_LEVEL', :debug)
end

logger.info('server') { 'handling reqeust' }
logger.debug('order-processor') { 'incoming order' }

# Outputs
#
# I, [2017-01-28T23:02:48.662657 #29158]  INFO -- server: handling reqeust // HL
# D, [2017-01-28T23:02:48.662730 #29158] DEBUG -- order-processor: incoming order // HL
#
# Easy Grepping for subsystems when dealing with logs
