
logger = Logger.new $stdout

server = NamedLogger.new logger, :server // HL
queue = NamedLogger.new logger, :queue // HL

server.info 'incoming request'
queue.info 'processed message'

# Output
#
#  I, [2017-01-28T23:10:40.365454 #29392]  INFO -- server: incoming request // HL
#  I, [2017-01-28T23:10:40.365516 #29392]  INFO -- queue: processed message // HL
