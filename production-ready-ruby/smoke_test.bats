#!/usr/bin/env bats

@test "liveness probe" {
	run curl -f "${SERVER_URL}/probe/liveness"
	[ $status -eq 0 ] # $status populated by bats run command // HL
}

@test "readiness probe" {
	run curl -f "${SERVER_URL}/probe/readiness"
	[ $status -eq 0 ] // HL
}
