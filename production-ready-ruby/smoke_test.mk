# Sentinel artifact representing some commands have been run.
# Every non-phone make target must create a file
ENVIRONMENT:=tmp/environment

# Boot everything for testing
$(ENVIRONMENT):
	bundle exec rackup -p 9292 // HL
	mkdir -p $(@D)
	touch $@

.PHONY: test-smoke
# Run a smoke test; depend on the $(ENVIRONMENT)
test-smoke: $(ENVIRONMENT)
	env SERVER_URL=http://localhost:9292 bats smoke_test.bats // HL
